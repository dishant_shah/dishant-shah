Summary

 This is a single page application based on a good search experience for a user. A user can use this web page to make searches on different topics based on  location and time period. So user can filter his search based on location and month of the year. Without logging in to the system user can make search query, but results from this would not be very optimum. If user registers or logs in to the system he will have more precise search results.

In back end I can use an Inverted index for the searching the data. Wherein, I store each unique word found in any document, and for each word store documents in which that word is present. This will help in fast full-text search. 


